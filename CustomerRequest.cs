﻿using System.ComponentModel;

namespace Lab3
{
    public enum TravelClass
    {
        [Description("First")]
        First,
        [Description("Second")]
        Second
    }

    public enum TravelType
    {
        [Description("One Way")]
        OneWay,
        [Description("Return")]
        Return
    }

    public enum DiscountType
    {
        [Description("None")]
        None,
        [Description("20%")]
        Twenty,
        [Description("40%")]
        Forty
    }

    public class CustomerRequest
    {
        public string Source { get; private set; }
        public string Destination { get; private set; }
        public TravelClass Class { get; private set; }
        public TravelType Type { get; private set; }
        public DiscountType Discount { get; private set; }
        public IPayment Payment { get; private set; }

        public CustomerRequest(string source, string destination, TravelClass travelClass, TravelType type, DiscountType discount, IPayment payment)
        {
            Source = source;
            Destination = destination;
            Class = travelClass;
            Type = type;
            Discount = discount;
            Payment = payment;
        }
    }
}
