﻿namespace Lab3
{
    partial class UI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxFrom = new System.Windows.Forms.ComboBox();
            this.comboBoxTo = new System.Windows.Forms.ComboBox();
            this.groupBoxDiscount = new System.Windows.Forms.GroupBox();
            this.radioButtonDiscount40 = new System.Windows.Forms.RadioButton();
            this.radioButtonDiscount20 = new System.Windows.Forms.RadioButton();
            this.radioButtonDiscountNone = new System.Windows.Forms.RadioButton();
            this.groupBoxClass = new System.Windows.Forms.GroupBox();
            this.radioButtonClass2 = new System.Windows.Forms.RadioButton();
            this.radioButtonClass1 = new System.Windows.Forms.RadioButton();
            this.groupBoxAmout = new System.Windows.Forms.GroupBox();
            this.radioButtonAmountReturn = new System.Windows.Forms.RadioButton();
            this.radioButtonAmountOneWay = new System.Windows.Forms.RadioButton();
            this.comboBoxPayment = new System.Windows.Forms.ComboBox();
            this.buttonPay = new System.Windows.Forms.Button();
            this.labelFrom = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.labelPayment = new System.Windows.Forms.Label();
            this.groupBoxDiscount.SuspendLayout();
            this.groupBoxClass.SuspendLayout();
            this.groupBoxAmout.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxFrom
            // 
            this.comboBoxFrom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.comboBoxFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxFrom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(204)))), ((int)(((byte)(51)))));
            this.comboBoxFrom.FormattingEnabled = true;
            this.comboBoxFrom.Location = new System.Drawing.Point(47, 8);
            this.comboBoxFrom.Name = "comboBoxFrom";
            this.comboBoxFrom.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFrom.TabIndex = 0;
            // 
            // comboBoxTo
            // 
            this.comboBoxTo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.comboBoxTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(204)))), ((int)(((byte)(51)))));
            this.comboBoxTo.FormattingEnabled = true;
            this.comboBoxTo.Location = new System.Drawing.Point(203, 8);
            this.comboBoxTo.Name = "comboBoxTo";
            this.comboBoxTo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTo.TabIndex = 1;
            // 
            // groupBoxDiscount
            // 
            this.groupBoxDiscount.Controls.Add(this.radioButtonDiscount40);
            this.groupBoxDiscount.Controls.Add(this.radioButtonDiscount20);
            this.groupBoxDiscount.Controls.Add(this.radioButtonDiscountNone);
            this.groupBoxDiscount.Location = new System.Drawing.Point(211, 35);
            this.groupBoxDiscount.Name = "groupBoxDiscount";
            this.groupBoxDiscount.Size = new System.Drawing.Size(113, 89);
            this.groupBoxDiscount.TabIndex = 2;
            this.groupBoxDiscount.TabStop = false;
            this.groupBoxDiscount.Text = "Discount";
            // 
            // radioButtonDiscount40
            // 
            this.radioButtonDiscount40.AutoSize = true;
            this.radioButtonDiscount40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonDiscount40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.radioButtonDiscount40.Location = new System.Drawing.Point(6, 64);
            this.radioButtonDiscount40.Name = "radioButtonDiscount40";
            this.radioButtonDiscount40.Size = new System.Drawing.Size(102, 17);
            this.radioButtonDiscount40.TabIndex = 2;
            this.radioButtonDiscount40.Text = "40% Discount";
            this.radioButtonDiscount40.UseVisualStyleBackColor = true;
            // 
            // radioButtonDiscount20
            // 
            this.radioButtonDiscount20.AutoSize = true;
            this.radioButtonDiscount20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonDiscount20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.radioButtonDiscount20.Location = new System.Drawing.Point(6, 41);
            this.radioButtonDiscount20.Name = "radioButtonDiscount20";
            this.radioButtonDiscount20.Size = new System.Drawing.Size(102, 17);
            this.radioButtonDiscount20.TabIndex = 1;
            this.radioButtonDiscount20.Text = "20% Discount";
            this.radioButtonDiscount20.UseVisualStyleBackColor = true;
            // 
            // radioButtonDiscountNone
            // 
            this.radioButtonDiscountNone.AutoSize = true;
            this.radioButtonDiscountNone.Checked = true;
            this.radioButtonDiscountNone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonDiscountNone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.radioButtonDiscountNone.Location = new System.Drawing.Point(6, 19);
            this.radioButtonDiscountNone.Name = "radioButtonDiscountNone";
            this.radioButtonDiscountNone.Size = new System.Drawing.Size(95, 17);
            this.radioButtonDiscountNone.TabIndex = 0;
            this.radioButtonDiscountNone.TabStop = true;
            this.radioButtonDiscountNone.Text = "No Discount";
            this.radioButtonDiscountNone.UseVisualStyleBackColor = true;
            // 
            // groupBoxClass
            // 
            this.groupBoxClass.Controls.Add(this.radioButtonClass2);
            this.groupBoxClass.Controls.Add(this.radioButtonClass1);
            this.groupBoxClass.Location = new System.Drawing.Point(11, 35);
            this.groupBoxClass.Name = "groupBoxClass";
            this.groupBoxClass.Size = new System.Drawing.Size(89, 89);
            this.groupBoxClass.TabIndex = 3;
            this.groupBoxClass.TabStop = false;
            this.groupBoxClass.Text = "Class";
            // 
            // radioButtonClass2
            // 
            this.radioButtonClass2.AutoSize = true;
            this.radioButtonClass2.Checked = true;
            this.radioButtonClass2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonClass2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.radioButtonClass2.Location = new System.Drawing.Point(6, 41);
            this.radioButtonClass2.Name = "radioButtonClass2";
            this.radioButtonClass2.Size = new System.Drawing.Size(80, 17);
            this.radioButtonClass2.TabIndex = 2;
            this.radioButtonClass2.TabStop = true;
            this.radioButtonClass2.Text = "2nd Class";
            this.radioButtonClass2.UseVisualStyleBackColor = true;
            // 
            // radioButtonClass1
            // 
            this.radioButtonClass1.AutoSize = true;
            this.radioButtonClass1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonClass1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.radioButtonClass1.Location = new System.Drawing.Point(6, 19);
            this.radioButtonClass1.Name = "radioButtonClass1";
            this.radioButtonClass1.Size = new System.Drawing.Size(76, 17);
            this.radioButtonClass1.TabIndex = 1;
            this.radioButtonClass1.Text = "1st Class";
            this.radioButtonClass1.UseVisualStyleBackColor = true;
            // 
            // groupBoxAmout
            // 
            this.groupBoxAmout.Controls.Add(this.radioButtonAmountReturn);
            this.groupBoxAmout.Controls.Add(this.radioButtonAmountOneWay);
            this.groupBoxAmout.Location = new System.Drawing.Point(106, 35);
            this.groupBoxAmout.Name = "groupBoxAmout";
            this.groupBoxAmout.Size = new System.Drawing.Size(99, 89);
            this.groupBoxAmout.TabIndex = 3;
            this.groupBoxAmout.TabStop = false;
            this.groupBoxAmout.Text = "Amout";
            // 
            // radioButtonAmountReturn
            // 
            this.radioButtonAmountReturn.AutoSize = true;
            this.radioButtonAmountReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonAmountReturn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.radioButtonAmountReturn.Location = new System.Drawing.Point(6, 42);
            this.radioButtonAmountReturn.Name = "radioButtonAmountReturn";
            this.radioButtonAmountReturn.Size = new System.Drawing.Size(63, 17);
            this.radioButtonAmountReturn.TabIndex = 1;
            this.radioButtonAmountReturn.Text = "Return";
            this.radioButtonAmountReturn.UseVisualStyleBackColor = true;
            // 
            // radioButtonAmountOneWay
            // 
            this.radioButtonAmountOneWay.AutoSize = true;
            this.radioButtonAmountOneWay.Checked = true;
            this.radioButtonAmountOneWay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonAmountOneWay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.radioButtonAmountOneWay.Location = new System.Drawing.Point(6, 19);
            this.radioButtonAmountOneWay.Name = "radioButtonAmountOneWay";
            this.radioButtonAmountOneWay.Size = new System.Drawing.Size(74, 17);
            this.radioButtonAmountOneWay.TabIndex = 0;
            this.radioButtonAmountOneWay.TabStop = true;
            this.radioButtonAmountOneWay.Text = "One-way";
            this.radioButtonAmountOneWay.UseVisualStyleBackColor = true;
            // 
            // comboBoxPayment
            // 
            this.comboBoxPayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.comboBoxPayment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPayment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(204)))), ((int)(((byte)(51)))));
            this.comboBoxPayment.FormattingEnabled = true;
            this.comboBoxPayment.Location = new System.Drawing.Point(104, 130);
            this.comboBoxPayment.Name = "comboBoxPayment";
            this.comboBoxPayment.Size = new System.Drawing.Size(220, 21);
            this.comboBoxPayment.TabIndex = 4;
            // 
            // buttonPay
            // 
            this.buttonPay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.buttonPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(204)))), ((int)(((byte)(51)))));
            this.buttonPay.Location = new System.Drawing.Point(11, 157);
            this.buttonPay.Name = "buttonPay";
            this.buttonPay.Size = new System.Drawing.Size(313, 23);
            this.buttonPay.TabIndex = 5;
            this.buttonPay.Text = "Pay";
            this.buttonPay.UseVisualStyleBackColor = false;
            // 
            // labelFrom
            // 
            this.labelFrom.AutoSize = true;
            this.labelFrom.Location = new System.Drawing.Point(8, 11);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(33, 13);
            this.labelFrom.TabIndex = 6;
            this.labelFrom.Text = "From:";
            // 
            // labelTo
            // 
            this.labelTo.AutoSize = true;
            this.labelTo.Location = new System.Drawing.Point(174, 11);
            this.labelTo.Name = "labelTo";
            this.labelTo.Size = new System.Drawing.Size(23, 13);
            this.labelTo.TabIndex = 7;
            this.labelTo.Text = "To:";
            // 
            // labelPayment
            // 
            this.labelPayment.AutoSize = true;
            this.labelPayment.Location = new System.Drawing.Point(8, 133);
            this.labelPayment.Name = "labelPayment";
            this.labelPayment.Size = new System.Drawing.Size(90, 13);
            this.labelPayment.TabIndex = 8;
            this.labelPayment.Text = "Payment Method:";
            // 
            // UI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(204)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(334, 191);
            this.Controls.Add(this.labelPayment);
            this.Controls.Add(this.labelTo);
            this.Controls.Add(this.labelFrom);
            this.Controls.Add(this.buttonPay);
            this.Controls.Add(this.comboBoxPayment);
            this.Controls.Add(this.groupBoxAmout);
            this.Controls.Add(this.groupBoxClass);
            this.Controls.Add(this.groupBoxDiscount);
            this.Controls.Add(this.comboBoxTo);
            this.Controls.Add(this.comboBoxFrom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "UI";
            this.Text = "MSO Label Exercise III";
            this.groupBoxDiscount.ResumeLayout(false);
            this.groupBoxDiscount.PerformLayout();
            this.groupBoxClass.ResumeLayout(false);
            this.groupBoxClass.PerformLayout();
            this.groupBoxAmout.ResumeLayout(false);
            this.groupBoxAmout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxFrom;
        private System.Windows.Forms.ComboBox comboBoxTo;
        private System.Windows.Forms.GroupBox groupBoxDiscount;
        private System.Windows.Forms.GroupBox groupBoxClass;
        private System.Windows.Forms.GroupBox groupBoxAmout;
        private System.Windows.Forms.ComboBox comboBoxPayment;
        private System.Windows.Forms.Button buttonPay;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.Label labelPayment;
        private System.Windows.Forms.RadioButton radioButtonDiscount40;
        private System.Windows.Forms.RadioButton radioButtonDiscount20;
        private System.Windows.Forms.RadioButton radioButtonDiscountNone;
        private System.Windows.Forms.RadioButton radioButtonClass2;
        private System.Windows.Forms.RadioButton radioButtonClass1;
        private System.Windows.Forms.RadioButton radioButtonAmountReturn;
        private System.Windows.Forms.RadioButton radioButtonAmountOneWay;
    }
}