﻿namespace Lab3
{
    class CardPaymentAdapter : IPayment
    {
        private readonly ICard _adaptee;

        public CardPaymentAdapter(ICard adaptee)
        {
            _adaptee = adaptee;
        }

        public bool HandlePayment(double price)
        {
            if (_adaptee.GetType() == typeof(CreditCard))
                price += 0.5;
            _adaptee.Connect();
            int id = _adaptee.BeginTransaction((float)price);
            _adaptee.EndTransaction(id);
            return true;
        }

        public override string ToString()
        {
            return _adaptee.ToString();
        }
    }
}
