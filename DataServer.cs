﻿using System;

namespace Lab3
{
    public static class DataServer
    {
        public static double CalculatePrice(CustomerRequest request)
        {
            int tableColumn;

            switch (request.Class) //Handle travel class
            {
                case TravelClass.First:
                    tableColumn = 3;
                    break;
                case TravelClass.Second:
                    tableColumn = 0;
                    break;
                default:
                    throw new Exception("Unknown TravelClass!");
            }

            switch (request.Discount) //Handle discount type
            {
                case DiscountType.None:
                    tableColumn += 0;
                    break;
                case DiscountType.Twenty:
                    tableColumn += 1;
                    break;
                case DiscountType.Forty:
                    tableColumn += 2;
                    break;
                default:
                    throw new Exception("Unknown DiscountType!");
            }

            double price = PricingTable.getPrice(
                Tariefeenheden.getTariefeenheden(request.Source, request.Destination), 
                tableColumn);

            switch (request.Type) //Handle return type
            {
                case TravelType.OneWay:
                    price *= 1;
                    break;
                case TravelType.Return:
                    price *= 2;
                    break;
                default:
                    throw new Exception("Unknown TravelType!");
            }

            return price;
        }
    }
}
