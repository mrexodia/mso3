﻿using System;

namespace Lab3
{
    class CashPaymentAdapter : IPayment
    {
        private readonly IKEAMyntAtare2000 _adaptee;

        public CashPaymentAdapter(IKEAMyntAtare2000 adaptee)
        {
            _adaptee = adaptee;
        }

        public bool HandlePayment(double price)
        {
            _adaptee.starta();
            _adaptee.betala((int)Math.Round(price * 100));
            _adaptee.stoppa();
            return true;
        }

        public override string ToString()
        {
            return "Cash";
        }
    }
}
