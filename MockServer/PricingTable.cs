﻿using System;

namespace Lab3
{
    public static class PricingTable
    {
        public static float getPrice(int tariefeenheden, int col)
        {
            double price = 0;

            switch (col)
            {
                case 0: //second class, no discount
                    price = 2.10;
                    break;
                case 1: //second class, twenty discount
                    price = 1.70;
                    break;
                case 2: //second class, forty discount
                    price = 1.30;
                    break;
                case 3: //first class, no discount
                    price = 3.60;
                    break;
                case 4: //first class, twenty discount
                    price = 2.90;
                    break;
                case 5: //first class, forty discount
                    price = 2.20;
                    break;
                default:
                    throw new Exception("Unknown column number");
            }

            price = price * 0.02 * tariefeenheden;

            return (float)Math.Round(price, 2);
        }
    }
}

