﻿namespace Lab3
{
    public static class TicketVendingMachine
    {
        public static TrainTicket GenerateTicket(CustomerRequest request)
        {
            return new TrainTicket(request.Source,
                request.Destination,
                request.Class,
                request.Discount,
                DataServer.CalculatePrice(request));
        }

        public static void HandlePayment(CustomerRequest request, IPayment payment)
        {
            var ticket = GenerateTicket(request);
            payment.HandlePayment(ticket.Price);
            Printer.PrintTicket(ticket);
        }
    }
}
