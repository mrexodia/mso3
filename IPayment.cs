﻿using System;

namespace Lab3
{
    public interface IPayment
    {
        bool HandlePayment(double price);
    }
}
