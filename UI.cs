﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab3
{
    public partial class UI : Form
    {
        private IPayment[] paymentMethods = new IPayment[]
        {
            new CardPaymentAdapter(new CreditCard()),
            new CardPaymentAdapter(new DebitCard()),
            new CashPaymentAdapter(new IKEAMyntAtare2000())
        };

        public UI()
        {
            InitializeComponent();

            var stations = Tariefeenheden.getStations();
            comboBoxFrom.Items.AddRange(stations);
            comboBoxFrom.SelectedIndex = 0;

            comboBoxTo.Items.AddRange(stations);
            comboBoxTo.SelectedIndex = 0;

            comboBoxPayment.Items.AddRange(paymentMethods);
            comboBoxPayment.SelectedIndex = 0;

            buttonPay.Click += (object sender, EventArgs e) => TicketVendingMachine.HandlePayment(
                getCustomerRequest(),
                comboBoxPayment.SelectedItem as IPayment);
        }

        private CustomerRequest getCustomerRequest()
        {
            var cls = radioButtonClass1.Checked ? TravelClass.First : TravelClass.Second;

            var type = radioButtonAmountOneWay.Checked ? TravelType.OneWay : TravelType.Return;

            var discount = DiscountType.None;
            if (radioButtonDiscount20.Checked)
                discount = DiscountType.Twenty;
            else if (radioButtonDiscount40.Checked)
                discount = DiscountType.Forty;

            return new CustomerRequest(
                comboBoxFrom.SelectedItem as string,
                comboBoxTo.SelectedItem as string,
                cls,
                type,
                discount,
                comboBoxPayment.SelectedItem as IPayment);
        }
    }
}
