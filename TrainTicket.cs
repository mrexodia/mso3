﻿using System;

namespace Lab3
{
    public class TrainTicket
    {
        public string Source { get; private set; }
        public string Destination { get; private set; }
        public TravelClass Class { get; private set; }
        public DiscountType Discount { get; private set; }
        public double Price { get; private set; }

        public TrainTicket(string source, string destination, TravelClass travelClass, DiscountType discount, double price)
        {
            Source = source;
            Destination = destination;
            Class = travelClass;
            Discount = discount;
            Price = price;
        }

        public override string ToString()
        {
            return string.Format(
                "From:\t{0}\nTo:\t{1}\nClass:\t{2}\nDiscount:\t{3}\nPrice:\t{4:0.00}",
                Source,
                Destination,
                Class.GetDescription(),
                Discount.GetDescription(),
                Price);
        }
    }
}
